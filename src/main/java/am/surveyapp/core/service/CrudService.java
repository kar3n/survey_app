package am.surveyapp.core.service;

import java.util.List;


public interface CrudService<Req, Resp> // TODO add comments
{
	List<Resp> findAll();
	
	Resp findById(long id);
	
	Resp saveOrUpdate(Req entity);
	
	Resp deleteById(long id);
}
