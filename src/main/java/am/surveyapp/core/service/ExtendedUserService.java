package am.surveyapp.core.service;

import am.surveyapp.core.model.dto.user.AppUserResponse;
import am.surveyapp.core.model.dto.user.AppUserSaveOrUpdateRequest;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface ExtendedUserService extends UserDetailsService, CrudService<AppUserSaveOrUpdateRequest, AppUserResponse> // TODO add comments
{
}
