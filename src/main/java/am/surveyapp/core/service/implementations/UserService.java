package am.surveyapp.core.service.implementations;

import am.surveyapp.core.model.dto.user.AppUserResponse;
import am.surveyapp.core.model.dto.user.AppUserSaveOrUpdateRequest;
import am.surveyapp.core.model.entity.AppUser;
import am.surveyapp.core.persistance.AppUserRepository;
import am.surveyapp.core.service.ExtendedUserService;
import am.surveyapp.core.util.mapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements ExtendedUserService //TODO add comments
{
	
	private AppUserRepository appUserRepository;
	private ModelMapper<AppUser, AppUserResponse> entityToUserResponseMapper;
	private ModelMapper userSoaToEntityMapper;
	private PasswordEncoder passwordEncoder;
	
	public UserService(AppUserRepository appUserRepository,
					   @Qualifier("entityToUserResponseMapper") ModelMapper<AppUser, AppUserResponse> entityToUserResponseMapper,
					   @Qualifier("userSouRequestToEntityMapper") ModelMapper userSoaToEntityMapper,
					   PasswordEncoder passwordEncoder)
	{
		this.appUserRepository          = appUserRepository;
		this.entityToUserResponseMapper = entityToUserResponseMapper;
		this.userSoaToEntityMapper      = userSoaToEntityMapper;
		this.passwordEncoder            = passwordEncoder;
	}
	
	@Override
	public List<AppUserResponse> findAll()
	{
		List<AppUser> appUsers = appUserRepository.findAll();
		return entityToUserResponseMapper.mapCollection(appUsers);
	}
	
	@Override
	public AppUserResponse findById(long id)
	{
		AppUser appUser = appUserRepository.findById(id).isPresent() ? appUserRepository.findById(id).get() : null;
		
		if (appUser == null) throw new RuntimeException(); // TODO change to custom exception
		return null; // TODO fix!
	}
	
	@Override
	public AppUserResponse saveOrUpdate(AppUserSaveOrUpdateRequest entity)
	{
		AppUser appUser = new AppUser();
		System.out.println(appUser);
		//appUserRepository.saveAndFlush()
		return null;
	}
	
	@Override
	public AppUserResponse deleteById(long id)
	{
		return null;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		return null;
	}
}
