package am.surveyapp.core.persistance;

import am.surveyapp.core.model.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> // TODO add comments
{
}
