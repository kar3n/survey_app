package am.surveyapp.core.persistance;

import am.surveyapp.core.model.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AppUserRepository extends JpaRepository<AppUser, Long> // TODO add comments
{

}
