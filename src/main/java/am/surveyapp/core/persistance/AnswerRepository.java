package am.surveyapp.core.persistance;

import am.surveyapp.core.model.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer,Long> // TODO add comments
{
}
