package am.surveyapp.core.persistance;

import am.surveyapp.core.model.entity.PassedSurvey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassedSurveyRepository extends JpaRepository<PassedSurvey, Long> // TODO add comments
{
}
