package am.surveyapp.core.model.dto.user;

import am.surveyapp.core.model.dto.BaseDto;
import am.surveyapp.core.model.entity.PassedSurvey;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppUserResponse extends BaseDto //TODO add comments and finalize
{
	private long id;
	
	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private List<PassedSurvey> passedSurveys;
}
