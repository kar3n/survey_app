package am.surveyapp.core.model.dto.user;

import am.surveyapp.core.model.dto.BaseDto;
import am.surveyapp.core.model.entity.PassedSurvey;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppUserSaveOrUpdateRequest extends BaseDto // TODO add comments and finalize
{
	
	@NotBlank(message = "First name required")
	private String firstName;
	
	@NotBlank(message = "Last name required")
	private String lastName;
	
	@NotBlank(message = "Username required")
	private String userName;
	
	@NotBlank(message = "Email address Required")
	@Email(message = "Invalid email address")
	private String email;
	
	@NotBlank(message = "Password required")
	@Min(value = 4, message = "Password minimum length is 4")
	private String password;
	
	@NotBlank(message = "Password confirmation required")
	private String passwordConfirmation;
	
	private List<PassedSurvey> passedSurveys;
	
	//private String adminCode; //TODO give solution to roles
}
