package am.surveyapp.core.model.dto.user;

import am.surveyapp.core.model.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppUserLoginRequest extends BaseDto // TODO add comments
{
	private String userName;
	private String password;
}
