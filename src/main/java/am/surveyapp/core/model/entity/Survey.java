package am.surveyapp.core.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Survey extends BaseEntity // TODO add comments
{
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date surveyCreationDate;
	
	@OneToOne(mappedBy = "survey", cascade = CascadeType.ALL)
	private PassedSurvey passedSurvey;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "survey_question", joinColumns = @JoinColumn(name = "survey_id"), inverseJoinColumns = @JoinColumn(name = "question_id"))
	private Set<Question> questions;
	
	
	
	
	
	
}
