package am.surveyapp.core.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;


@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppUser extends BaseEntity implements UserDetails //TODO finalize and comment
{
	
	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "username")
	private String userName;

	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;

	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "user_authorities", joinColumns = @JoinColumn(name = "id"))
	private List<UserAuthority> userAuthorities;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
	private List<PassedSurvey> passedSurveys;
	
	public AppUser()
	{
	}
	
	public AppUser(String firstName, String lastName, String userName, String email, String password,
				   List<UserAuthority> userAuthorities,
				   List<PassedSurvey> passedSurveys)
	{
		this.firstName       = firstName;
		this.lastName        = lastName;
		this.userName        = userName;
		this.email           = email;
		this.password        = password;
		this.userAuthorities = userAuthorities;
		this.passedSurveys   = passedSurveys;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return getUserAuthorities();
	}

	@Override
	public String getPassword()
	{
		return password;
	}

	@Override
	public String getUsername()
	{
		return userName;
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return false;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return false;
	}

	@Override
	public boolean isEnabled()
	{
		return false;
	}
	
}
