package am.surveyapp.core.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Question extends BaseEntity // TODO add comments
{
	private String questionText;
	
	@ManyToMany(mappedBy = "questions")
	private Set<Survey> surveys;
	
	@OneToMany(mappedBy = "question", fetch =FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Answer> answers;
}
