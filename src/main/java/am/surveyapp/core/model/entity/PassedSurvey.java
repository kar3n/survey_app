package am.surveyapp.core.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PassedSurvey extends BaseEntity // TODO add comments
{

	@Column(name = "title")
	private String title;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "survey_id")
	private Survey survey;
	
	@Column(name = "passed_date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date passedDate;
	
	@Column(name = "mark")
	private int mark;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private AppUser user;
}
