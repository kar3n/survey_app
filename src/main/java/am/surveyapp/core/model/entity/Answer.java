package am.surveyapp.core.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Answer extends BaseEntity
{
	
	@Column(name = "text_answer")
	private String textAnswer;
	
	@Column(name = "is_correct")
	private boolean isCorrect;
	
	@ManyToOne
	@JoinColumn(name = "question_id")
	private Question question;
}
