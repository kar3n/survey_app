package am.surveyapp.core.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


/**
 * Base Entity class for all Entities
 */
@MappedSuperclass
@Data
abstract class BaseEntity implements Serializable
{
	/**
	 * The id which should be mapped to inherited entity classes
	 * as id;
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
}
