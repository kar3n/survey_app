package am.surveyapp.core.model.entity;

import org.springframework.security.core.GrantedAuthority;

public enum UserAuthority implements GrantedAuthority // TODO add comments
{
	USER,
	ADMIN;
	
	@Override
	public String getAuthority()
	{
		return name();
	}
}
