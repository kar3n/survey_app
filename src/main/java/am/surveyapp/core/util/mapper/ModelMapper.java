package am.surveyapp.core.util.mapper;


import java.util.List;

public interface ModelMapper<S,D>
{
	D mapAndGet(S source);
	
	List<D> mapCollection(List<S> sourceCollection);
	
}
