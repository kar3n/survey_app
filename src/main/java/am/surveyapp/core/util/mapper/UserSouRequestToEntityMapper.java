package am.surveyapp.core.util.mapper;

import am.surveyapp.core.model.dto.user.AppUserSaveOrUpdateRequest;
import am.surveyapp.core.model.entity.AppUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class UserSouRequestToEntityMapper implements ModelMapper<AppUserSaveOrUpdateRequest, AppUser> //TODO add comments finalize
{
	
	@Override
	public AppUser mapAndGet(AppUserSaveOrUpdateRequest source)
	{
		return mapFromSourceToTarget(source);
	}
	
	@Override
	public List<AppUser> mapCollection(List<AppUserSaveOrUpdateRequest> sourceCollection)
	{
		List<AppUser> targetCollection = new ArrayList<>();
			sourceCollection.forEach(source -> targetCollection.add(mapFromSourceToTarget(source)));
		return targetCollection;
	}
	
	private AppUser mapFromSourceToTarget(AppUserSaveOrUpdateRequest source)
	{
		AppUser target = new AppUser();
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setEmail(source.getEmail());
		target.setUserName(source.getUserName());
		target.setPassedSurveys(source.getPassedSurveys());
		return target;
	}
}
