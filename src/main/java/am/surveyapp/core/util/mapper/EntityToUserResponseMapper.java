package am.surveyapp.core.util.mapper;

import am.surveyapp.core.model.dto.user.AppUserResponse;
import am.surveyapp.core.model.entity.AppUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class EntityToUserResponseMapper implements ModelMapper<AppUser, AppUserResponse> //TODO add comments
{
	@Override
	public AppUserResponse mapAndGet(AppUser source)
	{
		return mapFromSourceToTarget(source);
	}
	
	@Override
	public List<AppUserResponse> mapCollection(List<AppUser> sourceCollection)
	{
		List<AppUserResponse> targetCollection = new ArrayList<>();
		sourceCollection.forEach(source -> targetCollection.add(mapFromSourceToTarget(source)));
		return targetCollection;
	}
	
	
	private AppUserResponse mapFromSourceToTarget(AppUser source)
	{
		AppUserResponse target = new AppUserResponse();
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setEmail(source.getEmail());
		target.setPassedSurveys(source.getPassedSurveys());
		return target;
	}
}
