package am.surveyapp.core.controller;

import am.surveyapp.core.model.dto.user.AppUserResponse;
import am.surveyapp.core.service.ExtendedUserService;
import am.surveyapp.core.util.mapper.UserSouRequestToEntityMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
@Slf4j
@Controller
public class TestController
{
	@Autowired
	private ExtendedUserService userService;
	
	@GetMapping("/all")
	public String getAllUsers()
	{
		System.out.println(userService.findAll());
		return "index";
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView update(@PathVariable long id) //TODO get all params from front and save user here
	{
		ModelAndView modelAndView = new ModelAndView("index");
		AppUserResponse userResponse = userService.findById(id);
		System.out.println(userResponse + " ********* in controller update method");
		userResponse.setFirstName("CHANGED NAME");
		
		//modelAndView.addObject("changedUser", userService.saveOrUpdate(userResponse));
		UserSouRequestToEntityMapper mapper = new UserSouRequestToEntityMapper();
		//mapper.mapCollection(new ArrayList<AppUserSaveOrUpdateRequest>(), new ArrayList<>());
		return modelAndView;
	}
	
}
